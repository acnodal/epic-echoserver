


function getheaderinfo(req) {


    const headers = {

        host: req.get('Host'),
        httpVersion: req.httpVersion,
        url: req.url,
        method: req.method,
        xforwardedfor: req.get('x-forwarded-for'),
        xforwardedproto: req.get('x-forwarded-proto'),
        via: req.get('via')
    };

    return headers

}



module.exports = { getheaderinfo }