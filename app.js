const express = require("express");
const k8s = require("./k8s/kubeinfo.js");
const header = require("./headers/getheaderinfo.js");




const app = express();
app.set("view engine", "ejs");

app.use(express.static("public"));

app.get("/", async (req, res) => {

    //console.log(req);



    headers = header.getheaderinfo(req);

    gtw = await k8s.getinfo(process.env.POD_NAMESPACE);

  

    res.render("home", {header: headers, gtw: gtw });


});




app.listen(8080, () => {
    console.log("app now listening for requests on port 8080");
  });