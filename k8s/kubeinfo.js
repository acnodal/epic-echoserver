const fs = require('fs');
const os = require('os');
const k8s = require("@kubernetes/client-node");


const kc = new k8s.KubeConfig();
kc.loadFromDefault();


function getservice(namespace) {

    const k8sApi = kc.makeApiClient(k8s.CoreV1Api);

    const data = k8sApi.listNamespacedService(namespace);

    return data;
}

function gethttproute(namespace) {

    const k8sApi = kc.makeApiClient(k8s.CustomObjectsApi);

    const data = k8sApi.listNamespacedCustomObject(
        "gateway.networking.k8s.io",
        "v1alpha2",
        namespace,
        "httproutes"
    );
   return data;

}


function getgtw(namespace) {
    const k8sApi = kc.makeApiClient(k8s.CustomObjectsApi);

    const data = k8sApi.listNamespacedCustomObject(
        "gateway.networking.k8s.io",
        "v1alpha2",
        namespace,
        "gateways"
    );
   return data;
    
}



async function getinfo(namespace) {

    services = await getservice(namespace);

    let svclookup = ""

    const k8sservices = [];

    const k8shttprts = [];

    const k8sgtws = [];

    fs.readFile('/etc/podinfo/labels', 'utf8', (err, data) => {
        if (err) {
            console.error(err)
            return
        };
        (data.split(os.EOL)).forEach((labels) => {
            let label = labels.replace(/[=]/g, '":');
            label = '{"' + label + '}' ;

            services.body.items.forEach((svc) => {
                let selector = JSON.stringify(svc.spec.selector);

                if (selector == label ) {

                    const service = {
                        selector: label,
                        service: svc.metadata.name
                    }
                    svclookup = svc.metadata.name

                    k8sservices.push(service);
                }
            });
        });
    });

    httproutes = await gethttproute(namespace);


    httproutes.body.items.forEach((httproute) => {


        httproute.spec.rules.forEach((rules) => {
    
          rules.backendRefs.forEach((refs) => {
    

            if ((refs.kind == 'Service')  && (refs.name == svclookup)) {

                httproute.spec.parentRefs.forEach((prefs) => {

                    const httprt = {
                        httproute: httproute.metadata.name,
                        parentRef_gwname: prefs.name,
                        parentRef_gwns: prefs.namespace,
                        rules: rules.backendRefs
                    }
                    
                    k8shttprts.push(httprt);

                });
            }
            
          }) 
    
        });
    
      });

    
    gateways = await getgtw(namespace);

    k8shttprts.forEach((gateway) => {

       gateways.body.items.forEach((gate) => {


            if ( gateway.parentRef_gwname == gate.metadata.name) {
            

                const gtw = {
                    gatewayclassname: gate.spec.gatewayClassName,
                    listeners: gate.spec.listeners
                }
            
                k8sgtws.push(gtw);

            }
       })

    })



    return { k8sservices, k8shttprts, k8sgtws };
}


module.exports = { getinfo };

