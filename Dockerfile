# Container for epic-demo deployment

FROM node:17-bullseye

RUN npm install --global nodemon

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY --chown=node:node . .

USER node

EXPOSE 8080

CMD ["nodemon", "app.js"]